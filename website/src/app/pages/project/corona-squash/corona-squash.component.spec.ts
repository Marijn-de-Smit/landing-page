import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoronaSquashComponent } from './corona-squash.component';

describe('CoronaSquashComponent', () => {
  let component: CoronaSquashComponent;
  let fixture: ComponentFixture<CoronaSquashComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoronaSquashComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoronaSquashComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
