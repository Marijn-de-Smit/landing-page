import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { CookiesComponent } from './pages/cookies/cookies.component';
import { CookiePopupComponent } from './components/cookie-popup/cookie-popup.component';

import { ProjectsComponent } from './pages/projects/projects.component';
import { ContactComponent } from './pages/contact/contact.component';
import { HomeComponent, EnumToArrayPipe } from './pages/home/home.component';
import { PianoComponent } from './pages/project/piano/piano.component';
import { FootballComponent } from './pages/project/football/football.component';
import { SimonComponent } from './pages/project/simon/simon.component';
import { TowerComponent } from './pages/project/tower/tower.component';
import { TempleComponent } from './pages/project/temple/temple.component';
import { PrivacyComponent } from './pages/privacy/privacy.component';
import { PopUpComponent } from './components/pop-up/pop-up.component';
import { ProjectComponent } from './pages/project/project/project.component';
import { CoronaSquashComponent } from './pages/project/corona-squash/corona-squash.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ProjectsComponent,
    ContactComponent,
    HomeComponent,
    EnumToArrayPipe,
    PianoComponent,
    FootballComponent,
    SimonComponent,
    TowerComponent,
    TempleComponent,
    CookiesComponent,
    PopUpComponent,
    PrivacyComponent,
    CookiePopupComponent,
    ProjectComponent,
    CoronaSquashComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
