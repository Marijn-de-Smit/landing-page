import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(document).ready(function(){
      
      $(".ProjectToggle").click(function(e){
        e.preventDefault();

        if($(".Dropdown").hasClass("active")){
          $(".Dropdown").removeClass("active");
        }else{
          $(".Dropdown").addClass("active");
        }
      });
    });
  }

}
