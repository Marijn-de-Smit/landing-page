import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { ContactComponent } from './pages/contact/contact.component';
import { ProjectsComponent } from './pages/projects/projects.component';
import { ProjectComponent } from './pages/project/project/project.component';
import { PianoComponent } from './pages/project/piano/piano.component';
import { FootballComponent } from './pages/project/football/football.component';
import { SimonComponent } from './pages/project/simon/simon.component';
import { TowerComponent } from './pages/project/tower/tower.component';
import { TempleComponent } from './pages/project/temple/temple.component';
import { CookiesComponent } from './pages/cookies/cookies.component';
import { PrivacyComponent } from './pages/privacy/privacy.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'projects', component: ProjectsComponent},
  {path: 'project', component: ProjectComponent},
  {path: 'project/piano', component: PianoComponent},
  {path: 'project/football', component: FootballComponent},
  {path: 'project/simon', component: SimonComponent},
  {path: 'project/tower', component: TowerComponent},
  {path: 'project/temple', component: TempleComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'cookies', component: CookiesComponent},
  {path: 'privacy', component: PrivacyComponent},
  {path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
